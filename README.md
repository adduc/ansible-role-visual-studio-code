adduc.visual-studio-code
=========

[![build status](https://gitlab.128.io/adduc/ansible-role-visual-studio-code/badges/master/build.svg)](https://gitlab.128.io/adduc/ansible-role-visual-studio-code/commits/master)

This Ansible role installs Microsoft's Visual Studio Code.


OS Support
----

This role is actively tested against the following environments:

* Debian 8 (Jessie)
* Debian 9 (Stretch)
* Debian 10 (Sid)
* Ubuntu 14.04
* Ubuntu 16.04


This role should work in the following environments (but may not have 
been tested):

* Debian 8 (Jessie)
* Debian 9 (Stretch)
* Debian 10 (Sid)
* Ubuntu 14.04
* Ubuntu 14.10
* Ubuntu 15.04
* Ubuntu 15.10
* Ubuntu 16.04

Requirements
------------

This role has no pre-requisites.

Role Variables
--------------

* `code_version`: defaults to 1.2.1 (currently the latest)

Dependencies
------------

This role has no dependencies.

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: adduc.visual-studio-code }

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
